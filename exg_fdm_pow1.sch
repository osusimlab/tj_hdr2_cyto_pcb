EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 7 14
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L LT3042 U?
U 1 1 5CF9A0D4
P 4400 3300
AR Path="/5CF9A02D/5CF9A0D4" Ref="U?"  Part="1" 
AR Path="/5D0CD6DF/5CF9A0D4" Ref="U?"  Part="1" 
AR Path="/5D0CD6D4/5CF9A0D4" Ref="U?"  Part="1" 
AR Path="/5D0CD6C9/5CF9A0D4" Ref="U?"  Part="1" 
AR Path="/5D0D3123/5CF9A0D4" Ref="U?"  Part="1" 
F 0 "U?" H 4400 3797 60  0000 C CNN
F 1 "LT3042" H 4400 3691 60  0000 C CNN
F 2 "" H 0   -150 60  0001 C CNN
F 3 "" H 0   -150 60  0001 C CNN
	1    4400 3300
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 5CF9A1B6
P 3300 3200
AR Path="/5CF9A02D/5CF9A1B6" Ref="C?"  Part="1" 
AR Path="/5D0CD6DF/5CF9A1B6" Ref="C?"  Part="1" 
AR Path="/5D0CD6D4/5CF9A1B6" Ref="C?"  Part="1" 
AR Path="/5D0CD6C9/5CF9A1B6" Ref="C?"  Part="1" 
AR Path="/5D0D3123/5CF9A1B6" Ref="C?"  Part="1" 
F 0 "C?" H 3415 3246 50  0000 L CNN
F 1 "100n" H 3415 3155 50  0000 L CNN
F 2 "" H 1088 150 50  0001 C CNN
F 3 "" H 1050 300 50  0001 C CNN
	1    3300 3200
	1    0    0    -1  
$EndComp
$Comp
L C C?
U 1 1 5CF9A2A5
P 2800 3200
AR Path="/5CF9A02D/5CF9A2A5" Ref="C?"  Part="1" 
AR Path="/5D0CD6DF/5CF9A2A5" Ref="C?"  Part="1" 
AR Path="/5D0CD6D4/5CF9A2A5" Ref="C?"  Part="1" 
AR Path="/5D0CD6C9/5CF9A2A5" Ref="C?"  Part="1" 
AR Path="/5D0D3123/5CF9A2A5" Ref="C?"  Part="1" 
F 0 "C?" H 2915 3246 50  0000 L CNN
F 1 "4.7u" H 2915 3155 50  0000 L CNN
F 2 "" H 588 150 50  0001 C CNN
F 3 "" H 550 300 50  0001 C CNN
	1    2800 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 3050 3900 3050
Connection ~ 3300 3050
Wire Wire Line
	3900 3150 3700 3150
Wire Wire Line
	3700 3050 3700 3250
Connection ~ 3700 3050
Wire Wire Line
	3700 3250 3900 3250
Connection ~ 3700 3150
Wire Wire Line
	3300 3450 3300 3350
Wire Wire Line
	2800 3450 2800 3350
Connection ~ 3300 3450
Wire Wire Line
	2800 3450 3900 3450
NoConn ~ 3900 3350
Text HLabel 2600 3050 0    60   BiDi ~ 0
VDD_5p5
Connection ~ 2800 3050
Wire Wire Line
	4900 3450 5350 3450
Text Label 5350 3450 2    60   ~ 0
VDD_5p5
$Comp
L C C?
U 1 1 5CF9A0E3
P 5650 3850
AR Path="/5CF9A02D/5CF9A0E3" Ref="C?"  Part="1" 
AR Path="/5D0CD6DF/5CF9A0E3" Ref="C?"  Part="1" 
AR Path="/5D0CD6D4/5CF9A0E3" Ref="C?"  Part="1" 
AR Path="/5D0CD6C9/5CF9A0E3" Ref="C?"  Part="1" 
AR Path="/5D0D3123/5CF9A0E3" Ref="C?"  Part="1" 
F 0 "C?" H 5765 3896 50  0000 L CNN
F 1 "10u" H 5765 3805 50  0000 L CNN
F 2 "" H 3438 800 50  0001 C CNN
F 3 "" H 3400 950 50  0001 C CNN
	1    5650 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 3350 6100 3350
Wire Wire Line
	5650 3350 5650 3700
$Comp
L POT RV?
U 1 1 5CF9A341
P 6100 3600
AR Path="/5CF9A02D/5CF9A341" Ref="RV?"  Part="1" 
AR Path="/5D0CD6DF/5CF9A341" Ref="RV?"  Part="1" 
AR Path="/5D0CD6D4/5CF9A341" Ref="RV?"  Part="1" 
AR Path="/5D0CD6C9/5CF9A341" Ref="RV?"  Part="1" 
AR Path="/5D0D3123/5CF9A341" Ref="RV?"  Part="1" 
F 0 "RV?" H 6030 3646 50  0000 R CNN
F 1 "10k" H 6030 3555 50  0000 R CNN
F 2 "" H 1150 1000 50  0001 C CNN
F 3 "" H 1150 1000 50  0001 C CNN
	1    6100 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6100 3350 6100 3450
Connection ~ 5650 3350
Wire Wire Line
	6250 3600 6350 3600
Wire Wire Line
	6350 3600 6350 3850
Wire Wire Line
	6350 3850 6100 3850
Wire Wire Line
	6100 3750 6100 3950
$Comp
L R R?
U 1 1 5CF9A9D1
P 6100 4100
AR Path="/5CF9A02D/5CF9A9D1" Ref="R?"  Part="1" 
AR Path="/5D0CD6DF/5CF9A9D1" Ref="R?"  Part="1" 
AR Path="/5D0CD6D4/5CF9A9D1" Ref="R?"  Part="1" 
AR Path="/5D0CD6C9/5CF9A9D1" Ref="R?"  Part="1" 
AR Path="/5D0D3123/5CF9A9D1" Ref="R?"  Part="1" 
F 0 "R?" H 6170 4146 50  0000 L CNN
F 1 "10k" H 6170 4055 50  0000 L CNN
F 2 "" V 830 500 50  0001 C CNN
F 3 "" H 900 500 50  0001 C CNN
	1    6100 4100
	-1   0    0    -1  
$EndComp
Connection ~ 6100 3850
Wire Wire Line
	5650 4000 5650 4350
Wire Wire Line
	5350 4350 8650 4350
Wire Wire Line
	6100 4350 6100 4250
Wire Wire Line
	4900 3250 5350 3250
Wire Wire Line
	4900 3050 7350 3050
Wire Wire Line
	4900 3150 5250 3150
Wire Wire Line
	5250 3150 5250 3050
Connection ~ 5250 3050
Text Label 5350 3250 2    60   ~ 0
GND
$Comp
L R R?
U 1 1 5CF9B78D
P 6650 3600
AR Path="/5CF9A02D/5CF9B78D" Ref="R?"  Part="1" 
AR Path="/5D0CD6DF/5CF9B78D" Ref="R?"  Part="1" 
AR Path="/5D0CD6D4/5CF9B78D" Ref="R?"  Part="1" 
AR Path="/5D0CD6C9/5CF9B78D" Ref="R?"  Part="1" 
AR Path="/5D0D3123/5CF9B78D" Ref="R?"  Part="1" 
F 0 "R?" H 6720 3646 50  0000 L CNN
F 1 "10k" H 6720 3555 50  0000 L CNN
F 2 "" V 1380 0   50  0001 C CNN
F 3 "" H 1450 0   50  0001 C CNN
	1    6650 3600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6650 4350 6650 3750
Connection ~ 6100 4350
Wire Wire Line
	6650 3050 6650 3450
$Comp
L C C?
U 1 1 5CF9BB23
P 7050 3600
AR Path="/5CF9A02D/5CF9BB23" Ref="C?"  Part="1" 
AR Path="/5D0CD6DF/5CF9BB23" Ref="C?"  Part="1" 
AR Path="/5D0CD6D4/5CF9BB23" Ref="C?"  Part="1" 
AR Path="/5D0CD6C9/5CF9BB23" Ref="C?"  Part="1" 
AR Path="/5D0D3123/5CF9BB23" Ref="C?"  Part="1" 
F 0 "C?" H 7165 3646 50  0000 L CNN
F 1 "4.7u" H 7165 3555 50  0000 L CNN
F 2 "" H 4838 550 50  0001 C CNN
F 3 "" H 4800 700 50  0001 C CNN
	1    7050 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 4350 7050 3750
Connection ~ 6650 4350
Wire Wire Line
	7050 3050 7050 3450
Connection ~ 6650 3050
$Comp
L C C?
U 1 1 5CF9C1C3
P 7750 3600
AR Path="/5CF9A02D/5CF9C1C3" Ref="C?"  Part="1" 
AR Path="/5D0CD6DF/5CF9C1C3" Ref="C?"  Part="1" 
AR Path="/5D0CD6D4/5CF9C1C3" Ref="C?"  Part="1" 
AR Path="/5D0CD6C9/5CF9C1C3" Ref="C?"  Part="1" 
AR Path="/5D0D3123/5CF9C1C3" Ref="C?"  Part="1" 
F 0 "C?" H 7865 3646 50  0000 L CNN
F 1 "4.7u" H 7865 3555 50  0000 L CNN
F 2 "" H 5538 550 50  0001 C CNN
F 3 "" H 5500 700 50  0001 C CNN
	1    7750 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 3050 7750 3450
Wire Wire Line
	7750 4350 7750 3750
Connection ~ 7050 4350
$Comp
L CONN_01X02 J?
U 1 1 5CF9E0D1
P 7400 2850
AR Path="/5CF9A02D/5CF9E0D1" Ref="J?"  Part="1" 
AR Path="/5D0CD6DF/5CF9E0D1" Ref="J?"  Part="1" 
AR Path="/5D0CD6D4/5CF9E0D1" Ref="J?"  Part="1" 
AR Path="/5D0CD6C9/5CF9E0D1" Ref="J?"  Part="1" 
AR Path="/5D0D3123/5CF9E0D1" Ref="J?"  Part="1" 
F 0 "J?" H 7478 2891 50  0000 L CNN
F 1 "CONN_01X02" H 7478 2800 50  0000 L CNN
F 2 "" H 1050 350 50  0001 C CNN
F 3 "" H 1050 350 50  0001 C CNN
	1    7400 2850
	0    -1   -1   0   
$EndComp
Connection ~ 7050 3050
Wire Wire Line
	7450 3050 8900 3050
$Comp
L C C?
U 1 1 5CF9E35F
P 8200 3600
AR Path="/5CF9A02D/5CF9E35F" Ref="C?"  Part="1" 
AR Path="/5D0CD6DF/5CF9E35F" Ref="C?"  Part="1" 
AR Path="/5D0CD6D4/5CF9E35F" Ref="C?"  Part="1" 
AR Path="/5D0CD6C9/5CF9E35F" Ref="C?"  Part="1" 
AR Path="/5D0D3123/5CF9E35F" Ref="C?"  Part="1" 
F 0 "C?" H 8315 3646 50  0000 L CNN
F 1 "100n" H 8315 3555 50  0000 L CNN
F 2 "" H 5988 550 50  0001 C CNN
F 3 "" H 5950 700 50  0001 C CNN
	1    8200 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 3050 8200 3450
Wire Wire Line
	8200 4350 8200 3750
Connection ~ 7750 3050
Connection ~ 7750 4350
$Comp
L C C?
U 1 1 5CF9E7B7
P 8650 3600
AR Path="/5CF9A02D/5CF9E7B7" Ref="C?"  Part="1" 
AR Path="/5D0CD6DF/5CF9E7B7" Ref="C?"  Part="1" 
AR Path="/5D0CD6D4/5CF9E7B7" Ref="C?"  Part="1" 
AR Path="/5D0CD6C9/5CF9E7B7" Ref="C?"  Part="1" 
AR Path="/5D0D3123/5CF9E7B7" Ref="C?"  Part="1" 
F 0 "C?" H 8765 3646 50  0000 L CNN
F 1 "1n" H 8765 3555 50  0000 L CNN
F 2 "" H 6438 550 50  0001 C CNN
F 3 "" H 6400 700 50  0001 C CNN
	1    8650 3600
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 3050 8650 3450
Wire Wire Line
	8650 4350 8650 3750
Connection ~ 8200 4350
Connection ~ 8200 3050
Text HLabel 8900 3050 2    60   BiDi ~ 0
VDD
Connection ~ 8650 3050
$Comp
L BANA_PWR U?
U 1 1 5CFAE810
P 8800 2700
AR Path="/5CF9A02D/5CFAE810" Ref="U?"  Part="1" 
AR Path="/5D0CD6DF/5CFAE810" Ref="U?"  Part="1" 
AR Path="/5D0CD6D4/5CFAE810" Ref="U?"  Part="1" 
AR Path="/5D0CD6C9/5CFAE810" Ref="U?"  Part="1" 
AR Path="/5D0D3123/5CFAE810" Ref="U?"  Part="1" 
F 0 "U?" H 8977 2753 60  0000 L CNN
F 1 "BANA_PWR" H 8977 2647 60  0000 L CNN
F 2 "" H 50  1950 60  0001 C CNN
F 3 "" H 50  1950 60  0001 C CNN
	1    8800 2700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8800 2950 8800 3050
Connection ~ 8800 3050
Text HLabel 5350 4350 0    60   BiDi ~ 0
GND
Connection ~ 5650 4350
Text Label 3000 3450 0    60   ~ 0
GND
$EndSCHEMATC
